@ui @registration
Feature: User registration at test-my.umarkets.com

#  Scenario: user registers for Umarkets account
#    Given a web browser at "https://test-my.umarkets.com/#register"
#    When user register using email <email> and password <password>
#    Then user should be signed in
#    And page "My Account" should be visible


#  Scenario Outline: user registration for Umarkets account
#    Given a user at "Registration" page
#    When user register using email <email> and password <password>
#    Then user should be signed in
#    And page "My Account" should be visible
#    Examples:
#        | email              | password         |
##        | test2@mail.com     | Handmade         |
#        | #RANDOM-EMAIL      | #RANDOM-PASSWORD |
#        | #RANDOM-EMAIL      | #RANDOM-PASSWORD |
#        | #RANDOM-EMAIL      | #RANDOM-PASSWORD |


  Scenario: Client registration for Umarkets account
    Given A Client want to register with our service via "WEB UI"
    And He provides unique email address and password for registration
#    When He provides unique email address and password for registration
    When He submit registration data
# above step should store new user data
    Then He should be registered
#    And He should appear on "My Account" page

  Scenario: Newly registered Client should bew prompted to finish registration
    Given A newly registered Client logged in to a system
    When He navigates to any section of a site
    Then He should be prompted to finish registration

  Scenario: Newly registered Client should finish registration
    Given A newly registered Client logged in to a system
    When He prompted to finish registration
    Then He should fill form to finish registration