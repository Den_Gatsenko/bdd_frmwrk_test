@api @fast-reg-log1
Feature: simple API test using Serenity and Cucumber BDD via RestAssured

  @api:registration-ui
  Scenario: Client registration for Umarkets account via WEB UI
#    Given A Client want to register with our service via "REST API"
    Given A Client want to register with our service via "WEB UI"
    And He provides unique email address and password for registration
    When He submit registration data
# above step should store new user data
    Then He should be registered
#    And He should appear on "My Account" page

  Scenario: user logs in @ test-my.umarkets.com using appropriate email and password
    Given a web browser at "https://test-my.umarkets.com/#login"
    When user logs in using email "denisg@123software.ru" and password "q1w2e3r4t5"
    Then user should be signed in
    And page "My Account" should be visible


  @api:login-ui
  Scenario: Login via Umarkets UI
    Given A registered Client want to log in to Umarkets account via "WEB UI"
    When He submit login credentials
# above step should store new user data
    Then He should be logged in

  @api:login
  Scenario: Login via umarkets API
    Given A registered Client want to log in to Umarkets account via "REST API"
    When He submit login credentials
# above step should store new user data
    Then He should be logged in

