@api @fast-reg-log
Feature: simple API test using Serenity and Cucumber BDD via RestAssured

  @api:registration
  Scenario: Client registration for Umarkets account via WEB UI
#    Given A Client want to register with our service via "REST API"
    Given A Client want to register with our service via "REST API"
    And He provides unique email address and password for registration
    When He submit registration data
# above step should store new user data
    Then He should be registered
#    And He should appear on "My Account" page

  @api:login-ui
  Scenario: Login via Umarkets UI
    Given A registered Client want to log in to Umarkets account via "WEB UI"
    When He submit login credentials
# above step should store new user data
    Then He should be logged in

  @api:login
  Scenario: Login via umarkets API
    Given A registered Client want to log in to Umarkets account via "REST API"
    When He submit login credentials
# above step should store new user data
    Then He should be logged in

