@ui @login
Feature: log in to test-my.umarkets.com

  @ui:login
  Scenario: user logs in @ test-my.umarkets.com using appropriate email and password
    Given a web browser at "https://test-my.umarkets.com/#login"
    When user logs in using email "denisg@123software.ru" and password "q1w2e3r4t5"
    Then user should be signed in
    And page "My Account" should be visible


  @ui:login
  Scenario: Registered Client logs in with correct credentials
    Given A registered Client want to log in to Umarkets account via "REST API"
    When He submit login credentials
# above step should store new user data
    Then He should be logged in
#    And He should appear on "My Account" page

#  Scenario: login with proper credentials
#    Given user at "Sign In" page
#    When user logs in using email "denisg@123software.ru" and password "q1w2e3r4t5"
#    Then user should be signed in
#    And user at "My Account" page