@api
Feature: simple API test using Serenity and Cucumber BDD via RestAssured

  Scenario: User of umarkets API makes request to https://test-api.umarkets.com/Feeds/favorites
    Given user use API at base URL "https://test-api.umarkets.com"
    When request is made to endpoint "/Feeds/favorites"
    Then user should receive a list of available feeds

  Scenario: Login via umarkets API
    Given user use API at base URL "https://test-api.umarkets.com"
    When request is made to endpoint "/Feeds/favorites"
    Then user should receive a list of available feeds
