@ui @login-trading
Feature: log in to test-trading.umarkets.com

#  @ui:login-trading
  #Background: Client logs in @ test-trading.umarkets.com using appropriate email and password
  Background:
    Given a Client is on "Login" page
    When he logs in as "don"/"password"
    Then Client should be logged in

  @ui:login-trading
  Scenario: Client, logged in to Trading account, should be able to set hLine on Chart
    Given a logged in Client
    When Client checks current rate
    Then Client sets hLine "50" point above current rate