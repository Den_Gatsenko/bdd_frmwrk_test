package com.umarkets.bdd.models;

public class OpenCvContoursModel {
    int contourMinWidth = 1;
    int contourMinHeight = 1;
    int contourMinArea = 1;

    public OpenCvContoursModel(int contourMinArea) {
        this.contourMinArea = contourMinArea;
    }

    public OpenCvContoursModel(int contourMinWidth, int contourMinHeight) {
        this.contourMinWidth = contourMinWidth;
        this.contourMinHeight = contourMinHeight;
        updateArea();
    }

    @Override
    public String toString() {
        return "OpenCvContoursModel{" +
                "contourMinWidth=" + contourMinWidth +
                ", contourMinHeight=" + contourMinHeight +
                ", contourMinArea=" + contourMinArea +
                '}';
    }

    private void updateArea(){
        contourMinArea = contourMinHeight * contourMinWidth;
    }

    public int getContourMinWidth() {
        return contourMinWidth;
    }

    public void setContourMinWidth(int contourMinWidth) {
        this.contourMinWidth = contourMinWidth;
        updateArea();
    }

    public int getContourMinHeight() {
        return contourMinHeight;
    }

    public void setContourMinHeight(int contourMinHeight) {
        this.contourMinHeight = contourMinHeight;
        updateArea();
    }

    public int getContourMinArea() {
        return contourMinArea;
    }
}
