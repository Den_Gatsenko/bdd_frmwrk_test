package com.umarkets.bdd.models;

import org.opencv.core.Scalar;

public class OpenCvHsvModel {
    int hueStart = 0;
    int saturationStart = 0;
    int valueStart = 0;

    int hueStop = 180;
    int saturationStop = 255;
    int valueStop = 255;

    public OpenCvHsvModel(int hueStart, int saturationStart, int valueStart, int hueStop, int saturationStop, int valueStop) {
        this.hueStart = hueStart;
        this.saturationStart = saturationStart;
        this.valueStart = valueStart;
        this.hueStop = hueStop;
        this.saturationStop = saturationStop;
        this.valueStop = valueStop;
    }

    public int getHueStart() {
        return hueStart;
    }

    public void setHueStart(int hueStart) {
        this.hueStart = hueStart;
    }

    public int getSaturationStart() {
        return saturationStart;
    }

    public void setSaturationStart(int saturationStart) {
        this.saturationStart = saturationStart;
    }

    public int getValueStart() {
        return valueStart;
    }

    public void setValueStart(int valueStart) {
        this.valueStart = valueStart;
    }

    public int getHueStop() {
        return hueStop;
    }

    public void setHueStop(int hueStop) {
        this.hueStop = hueStop;
    }

    public int getSaturationStop() {
        return saturationStop;
    }

    public void setSaturationStop(int saturationStop) {
        this.saturationStop = saturationStop;
    }

    public int getValueStop() {
        return valueStop;
    }

    public void setValueStop(int valueStop) {
        this.valueStop = valueStop;
    }
}
