package com.umarkets.bdd.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features"
        , glue = {"com.umarkets.bdd.hooks",
        "com.umarkets.bdd.step_defs",
        "com.umarkets.bdd.step_defs.steps",
        "com.umarkets.bdd.pages"
}
)
public class AcceptanceTests {}
