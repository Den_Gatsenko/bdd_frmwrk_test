package com.umarkets.bdd.runner;

import com.umarkets.bdd.helpers.UsersStorage;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import static com.umarkets.bdd.helpers.Randomizer.getRandomEmail;
import static com.umarkets.bdd.helpers.Randomizer.getRandomPassword;
import static com.umarkets.bdd.helpers.UsersStorage.getUserById;
import static com.umarkets.bdd.helpers.UsersStorage.storeNewUser;
import static junit.framework.TestCase.assertEquals;

@RunWith(JUnit4.class)
public class UserStorageTests {


    HashMap AUTHOR_BOOK_MAP = new HashMap<Object, Object>() {
        {
            put("Dan Simmons", "Hyperion");
            put("Douglas Adams", "The Hitchhiker's Guide to the Galaxy");
        }
    };
    String[] HEADERS = { "author", "title"};


    @Test
    public void createCSVFile() throws IOException {
        FileWriter out = new FileWriter("book_new.csv");
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader(HEADERS))) {
            AUTHOR_BOOK_MAP.forEach((author, title) -> {
                try {
                    printer.printRecord(author, title);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Test
    public void givenCSVFile_whenRead_thenContentsAsExpected() throws IOException {
        Reader in = new FileReader("book_new.csv");
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withHeader(HEADERS)
                .withFirstRecordAsHeader()
                .parse(in);
        for (CSVRecord record : records) {
            String author = record.get("author");
            String title = record.get("title");
            assertEquals(AUTHOR_BOOK_MAP.get(author), title);
        }
    }


    @Test
    public void testUserStorageStore() throws IOException {
        UsersStorage.USER_STORAGE_DATA newUser = new UsersStorage.USER_STORAGE_DATA(getRandomEmail(), getRandomPassword());
        System.out.println("new user : " + newUser.toString());
        storeNewUser(newUser);
    }

    @Test
    public void testUserStorageRead() throws IOException {
        UsersStorage.USER_STORAGE_DATA newUser = getUserById(4);
        System.out.println("new user : " + newUser.toString());
    }
}
