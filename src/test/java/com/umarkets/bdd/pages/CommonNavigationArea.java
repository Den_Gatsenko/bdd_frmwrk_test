package com.umarkets.bdd.pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class CommonNavigationArea extends BasePage {

    public void openMyAccount(){
        navigateTo("My Account");
    }

    public void openDeposit(){
        navigateTo("Deposit");
    }

    public void openWithdrawal(){
        navigateTo("Withdrawal");
    }

    public void openPersonalData(){
        navigateTo("Personal Data");
    }

    public void openAccountStatus(){
        navigateTo("Account Status");
    }

    public void openDocuments(){
        navigateTo("Documents");
    }

    public void signOut(){
        navigateTo("Sign Out");
    }

    public void navigateTo(String area){
        find(By.linkText(area)).click();
    }


    /*
driver.get(baseUrl + "/#myaccount");
driver.findElement(By.linkText("My Account")).click();

driver.findElement(By.linkText("Deposit")).click();
driver.get(baseUrl + "/#deposit");

driver.findElement(By.linkText("Withdrawal")).click();
driver.get(baseUrl + "/#withdrawal");

driver.findElement(By.linkText("Personal Data")).click();
driver.get(baseUrl + "/#personal");

driver.findElement(By.linkText("Account Status")).click();
driver.get(baseUrl + "/#account-status");

driver.findElement(By.linkText("Documents")).click();
driver.get(baseUrl + "/#documents");

driver.findElement(By.linkText("Sign Out")).click();

     */
}
