package com.umarkets.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;

public class TradingPage extends BasePage {

    public void locateTradingCanvasInToView(){

        getDriver().manage().window().setSize(new Dimension(2222,2222));
        //Maximizing Chrome App Window.
//        JavascriptExecutor executor = (JavascriptExecutor) tradingPage.getDriver();
        getJavascriptExecutorFacade().executeScript("window.resizeTo(2222, 2222);");
//        getJavascriptExecutorFacade().executeScript("document.querySelector('table th:last-child').scrollIntoView();");
        getJavascriptExecutorFacade().executeScript("arguments[0].scrollIntoView(true)", chartsCanvasContainer);
    }


    @FindBy(css = "span.ui-icon.ui-icon-carat-1-s")
    WebElementFacade tabsSalesCollapseButton;
    @FindBy(css = "span.ui-icon.ui-icon-carat-1-n")
    WebElementFacade tabsSalesExpandButton;
    public void collapseExpandSalesBlock(boolean collapse){
        if (collapse) {
            tabsSalesCollapseButton.click();
        } else {
            tabsSalesExpandButton.click();
        }
    }

    @FindBy(css = "img.expandChartClass")
    WebElementFacade chartExpandButton;
    public void collapseExpandChartBlock(){
        chartExpandButton.click();
    }


    @FindBy(id = "hLine")
    WebElementFacade hLineButton;
    public void selectHLineTool(){ hLineButton.click(); }
    @FindBy(id = "vLine")
    WebElementFacade vLineButton;
    public void selectVLineTool(){ vLineButton.click(); }
    @FindBy(id = "tLine")
    WebElementFacade tLineButton;
    public void selectTLineTool(){ tLineButton.click(); }
    @FindBy(id = "pLine")
    WebElementFacade pLineButton;
    public void selectPLineTool(){ pLineButton.click(); }
    @FindBy(id = "hideShowAllLines")
    WebElementFacade hideShowAllLinesButton;
    public void hideShowAllLines(){ hideShowAllLinesButton.click(); }
    @FindBy(id = "settingsLines")
    WebElementFacade settingsLinesButton;
    public void openLinesSettings(){ settingsLinesButton.click(); }

    @FindBy(id = "chart-1")
    WebElementFacade chartsCanvasContainer;
    List<WebElementFacade> chartsCanvasList;
    @FindBy(className = "chart-grid")
    WebElementFacade chartGridCanvas;
    @FindBy(className = "chart-navigator")
    WebElementFacade chartNavigatorCanvas;
    @FindBy(className = "chart-data")
    WebElementFacade chartDataCanvas;
    @FindBy(className = "chart-pattern")
    WebElementFacade chartPatternCanvas;
    @FindBy(className = "chart-lines")
    WebElementFacade chartLinesCanvas;
    @FindBy(className = "chart-rate")
    public
    WebElementFacade chartRateCanvas;
    @FindBy(className = "chart-info")
    WebElementFacade chartInfoCanvas;

    @FindBy(className = "sgnl-chart")
    public
    WebElementFacade signalIndicatorCanvas;




    public void hideShowElementsExcept(List<WebElement> except, boolean hide) {
        for (WebElement el : chartsCanvasContainer.findElements(By.tagName("canvas"))) {
            if ( ! except.contains(el) ) {
                hideShowElement(el, hide);
            }
        }
    }

    public void hideShowElement(WebElement element, boolean hide) {
//        WebElement element = driver.findElement(By.xpath(xpath));
        String param = (hide ? "'hidden'" : "'visible'");
        getJavascriptExecutorFacade().executeScript("arguments[0].style.visibility=" + param, element);
//        ((JavascriptExecutor) driver).executeScript("arguments[0].style.visibility=" + param, element);
    }



}
