package com.umarkets.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.At;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

//@DefaultUrl("https://test-my.umarkets.com/#complete-registration")
@At("https://test-my.umarkets.com/#complete-registration")
public class CompleteRegistrationPage extends BasePage{

    @FindBy(name = "firstName")
    WebElementFacade firstNameField;

    @FindBy(name = "lastName")
    WebElementFacade lastNameField;

    @FindBy(name = "countryId")
    WebElementFacade countryIdSelect;

    @FindBy(name = "city")
    WebElementFacade cityField;

    @FindBy(name = "phone1.countryCode")
    WebElementFacade phone1countryCodeField;

    @FindBy(name = "phone1.number")
    WebElementFacade phone1numberField;

    @FindBy(name = "areTermsAccepted")
    WebElementFacade areTermsAcceptedCheckbox;

    @FindBy(css = "button.btn.btn-continue")
    WebElementFacade continueButton;

//    public void verifyOnCompleteRegistrationP

    public List<WebElement> getCountryOptions(){
        return (new Select(countryIdSelect)).getOptions();
    }

    public void setRandomCountry(){
        List<WebElement> sel = getCountryOptions();
        int rnd = (new Random()).nextInt(sel.size()-1);
        countryIdSelect.selectByIndex(rnd);
    }


    @FindBy(css = "h4.page-title")
    WebElementFacade completeRegistrationTitle;

    public void isOnCompleteRegistrationPage(){
        assertEquals("Complete Registration", completeRegistrationTitle.getText());
    }

    public void clickContinue(){
        continueButton.click();
    }

    public void setTermsAccepted(boolean accepted){
        setCheckbox(areTermsAcceptedCheckbox, accepted);
    }

    public void setPhoneNumber(String phoneNumber){
        enter(phoneNumber).into(phone1numberField);
    }

    public void setPhoneCountryCode(String countryCode){
        enter(countryCode).into(phone1countryCodeField);
    }

    public void setCityField(String city){
        enter(city).into(cityField);
    }

    public void setCountryIdSelect(String country){
        selectFromDropdown(countryIdSelect, country);
    }

    public void setLastNameField(String lastName){
        enter(lastName).into(lastNameField);
    }

    public void setFirstNameField(String firstName){
        enter(firstName).into(firstNameField);
    }

//    public CompleteRegistrationPage(WebDriver driver) {
//        super(driver);
//    }

}
