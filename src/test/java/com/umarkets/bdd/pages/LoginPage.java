package com.umarkets.bdd.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.yecht.Data;

import static org.junit.Assert.assertEquals;

@DefaultUrl("https://test-my.umarkets.com/#login")
public class LoginPage extends BasePage {

    @FindBy(css = "h4")
    WebElement signInHeader;

    @FindBy(xpath = "//button[@type='submit']", timeoutInSeconds="10")
    WebElement logInButton;

    @FindBy(id = "email", timeoutInSeconds="10")
    WebElement emailField;

    @FindBy(id = "password", timeoutInSeconds="10")
    WebElement passwordField;


//    @WhenPageOpens
//    public void waitUntilTitleAppears() {
//        element(logInButton).waitUntilVisible();
//    }

    public void setEmailField ( String email ){
        enter(email).into(emailField);
    }

    public void setPasswordField (String password) {
        enter(password).into(passwordField);
    }

    public void submitLogin() {
        logInButton.click();
    }


}
