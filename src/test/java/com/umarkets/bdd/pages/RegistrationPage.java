package com.umarkets.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

@DefaultUrl("https://test-my.umarkets.com/#register")
public class RegistrationPage extends BasePage {

    @FindBy(id = "email")
    WebElementFacade emailField;

    @FindBy(id = "password")
    WebElementFacade passwordField;

    @FindBy(id = "confirm")
    WebElementFacade passwordConfirmField;

    @FindBy(css = "button.btn.btn-success")
    WebElementFacade submitButton;

//    public RegistrationPage(WebDriver driver) {
//        super(driver);
//    }

    public void setEmailField(String userEmail){
//        waitFor(userEmail).to
        emailField.waitUntilEnabled();
        enter(userEmail).into(emailField);
    }

    public void setPasswordField(String userPassword){
        enter(userPassword).into(passwordField);
    }

    public void setConfirmPasswordField(String userPassword){
        enter(userPassword).into(passwordConfirmField);
    }

    public void submit(){
        submitButton.click();
    }


    /*
driver.get(baseUrl + "/?lang=en#register");
driver.findElement(By.id("")).clear();
driver.findElement(By.id("email")).sendKeys("test-bdd@test.com");
driver.findElement(By.id("")).clear();
driver.findElement(By.id("password")).sendKeys("q1w2e3r4t5");
driver.findElement(By.id("")).clear();
driver.findElement(By.id("confirm")).sendKeys("q1w2e3r4t5");
driver.findElement(By.cssSelector("")).click();

     */
}
