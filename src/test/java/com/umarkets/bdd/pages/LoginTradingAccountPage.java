package com.umarkets.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://test-trading.umarkets.com/login.html?v=1&op=logout&lang=en")
public class LoginTradingAccountPage extends BasePage {
    @FindBy(css = "h4")
    WebElement signInHeader;

    @FindBy(id = "btLogin", timeoutInSeconds="10")
    WebElement logInButton;

    @FindBy(id = "login", timeoutInSeconds="10")
    WebElement emailField;

    @FindBy(id = "password", timeoutInSeconds="10")
    WebElement passwordField;


    @FindBy(className = "userText", timeoutInSeconds="10")
    WebElement wellcomeUserText;


    public void setEmailField ( String email ){
        enter(email).into(emailField);
    }

    public void setPasswordField (String password) {
        enter(password).into(passwordField);
    }

    public void submitLogin() {
        logInButton.click();
    }

    public String getWellcomeUserText(){
        return wellcomeUserText.getText();
    }
}
