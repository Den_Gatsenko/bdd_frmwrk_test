package com.umarkets.bdd.helpers;

import org.yecht.Data;

import static io.qala.datagen.RandomValue.between;
import static io.qala.datagen.RandomValue.length;

public class Randomizer {

    public static String parseTemplate(String template){
        switch (template) {
            case "#RANDOM-EMAIL" : return getRandomEmail();
            case "#RANDOM-PASSWORD" : return getRandomPassword();
            default: return template;
        }
    }

    public static String getRandomEmail(){
        return between(5, 10).alphanumeric() + "@" + between(5, 10).english() + ".com";
    }

    public static UsersStorage.USER_STORAGE_DATA getRandomUser(){
        return new UsersStorage.USER_STORAGE_DATA(getRandomEmail(), getRandomPassword());
    }

    public static String getRandomPassword(){
        return between(8, 10).alphanumeric()+"1";
    }

    public static String getRandomName(){
        return between(4, 10).english();
    }

    public static String getRandomNumber(int length) {
        return length(length).numeric();
    }
}
