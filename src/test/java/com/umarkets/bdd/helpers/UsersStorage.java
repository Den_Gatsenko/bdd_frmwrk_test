package com.umarkets.bdd.helpers;

import com.google.common.collect.Iterables;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.yecht.Data;

import java.io.*;
import java.util.stream.StreamSupport;

public class UsersStorage {

    static final String REGISTERED_USERS_STORAGE_FILE = "new_users.csv";
    static final String REGISTERED_USERS_STORAGE_FILE_PATH = "";

    enum USERS_STORAGE_FILE_HEADERS {
        id, email, password
    }

    public static class USER_STORAGE_DATA {
        private String email;
        private String password;

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public USER_STORAGE_DATA(String email, String password) {
            this.email = email;
            this.password = password;
        }

        @Override
        public String toString() {
            return "USER_STORAGE_DATA{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }

    public static USER_STORAGE_DATA getUserById(long id){
        USER_STORAGE_DATA res = null;
        if ( id <= getUsersStorageRecordsCount() ){
            try ( Reader in = new FileReader(REGISTERED_USERS_STORAGE_FILE_PATH + REGISTERED_USERS_STORAGE_FILE) ){
                Iterable<CSVRecord> records = CSVFormat.DEFAULT
                        .withHeader(USERS_STORAGE_FILE_HEADERS.class).parse(in);
                CSVRecord record = Iterables.get(records, (int) id);
                res = new USER_STORAGE_DATA(record.get(USERS_STORAGE_FILE_HEADERS.email),
                                            record.get(USERS_STORAGE_FILE_HEADERS.password) );
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            throw new RuntimeException("No user with ID="+id+" in Users storage");

        return res;
    }

    public static void storeNewUser(USER_STORAGE_DATA user){
        storeNewUser(user.getEmail(), user.getPassword());
    }

    public static void storeNewUser(String email, String password){
        if ( !isUsersStorageFileExists() ){
            createUsersStorageFile();
        }
        long id = getUsersStorageRecordsCount() + 1;
        try ( FileWriter out = new FileWriter(REGISTERED_USERS_STORAGE_FILE_PATH + REGISTERED_USERS_STORAGE_FILE, true) ) {
//            try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(USERS_STORAGE_FILE_HEADERS.class))) {
            try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)){//, CSVFormat.DEFAULT.withHeader(USERS_STORAGE_FILE_HEADERS.class))) {
                printer.printRecord(id, email, password);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static long getUsersStorageRecordsCount(){
        try ( Reader in = new FileReader(REGISTERED_USERS_STORAGE_FILE_PATH + REGISTERED_USERS_STORAGE_FILE) ){
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withHeader(USERS_STORAGE_FILE_HEADERS.class)
                    .withFirstRecordAsHeader()
                    .parse(in);
            return StreamSupport.stream(records.spliterator(), false).count();
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            throw new RuntimeException("No Users Storage exists at : " + REGISTERED_USERS_STORAGE_FILE_PATH + REGISTERED_USERS_STORAGE_FILE );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static boolean isUsersStorageFileExists(){
        File f = new File ( REGISTERED_USERS_STORAGE_FILE_PATH + REGISTERED_USERS_STORAGE_FILE );
        if(f.exists() && !f.isDirectory()) {
            return true;
        } else
            return false;
    }

    private static void createUsersStorageFile(){
        try ( FileWriter out = new FileWriter(REGISTERED_USERS_STORAGE_FILE_PATH + REGISTERED_USERS_STORAGE_FILE ) ){
            CSVPrinter printer = CSVFormat.DEFAULT
                    .withHeader( USERS_STORAGE_FILE_HEADERS.class ).print(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
