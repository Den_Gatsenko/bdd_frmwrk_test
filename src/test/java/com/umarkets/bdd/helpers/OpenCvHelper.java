package com.umarkets.bdd.helpers;

import com.umarkets.bdd.models.OpenCvHsvModel;
//import cucumber.api.java8.Fi;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.photography.bluring.AnnotatedBluring;
import net.thucydides.core.model.screenshots.Screenshot;
import net.thucydides.core.steps.BaseStepListener;
import net.thucydides.core.steps.StepEventBus;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.*;
import org.openqa.selenium.Point;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

public class OpenCvHelper {

    static class Test {
        public static void loadOpenCVNativeLibrary() {
            nu.pattern.OpenCV.loadShared();
        }
    }

    static {
        Test.loadOpenCVNativeLibrary();
//        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

//        System.loadLibrary(System.getProperty("user.dir") + "/lib/" + Core.NATIVE_LIBRARY_NAME + ".dll");
    }

    public static String getOcrResult(String imgPath) {
        BytePointer outText;
        String res = "";

        tesseract.TessBaseAPI api = new tesseract.TessBaseAPI();
        // Initialize tesseract-ocr with English, without specifying tessdata path
//        if (api.Init(null, "eng") != 0) {
        if (api.Init(null, "eng") != 0) {
            System.err.println("Could not initialize tesseract.");
            System.exit(1);
        }

//        api. setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
//        api.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "!?@#$%&*()<>_-+=/:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
//        api.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, ".,0123456789");
//        api.setVariable("classify_bln_numeric_mode", "1");
        tesseract.TessBaseAPISetVariable(api, "tessedit_char_blacklist", "!?@#$%&*()<>_-+=/:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        tesseract.TessBaseAPISetVariable(api, "tessedit_char_whitelist", ".,0123456789");
        tesseract.TessBaseAPISetVariable(api, "classify_bln_numeric_mode", "1");
        // Open input image with leptonica library
        lept.PIX image = pixRead(imgPath);
        api.SetImage(image);
        // Get OCR result
        outText = api.GetUTF8Text();
        res = outText.getString().replaceAll("\\s", "");
        System.out.println("OCR output:\n String: " + outText.getString() + "\nFloat: " + outText.getFloat());

        // Destroy used object and release memory
        api.End();
        outText.deallocate();
        pixDestroy(image);


        return res;
    }

    public static void saveMatToImageFile(Mat src, String path){
        Imgcodecs.imwrite(path, src);
    }

    public static Mat convertToBinaryImage(Mat src){
        // Converting to binary image...
        Mat dst = new Mat();
//        Imgproc.threshold(src, dst, 200, 500, Imgproc.THRESH_BINARY_INV);
        Imgproc.threshold(src, dst, 120, 255, Imgproc.THRESH_BINARY);
//        Imgcodecs.imwrite("./image_crop_threshold_smpl.png", dst);

        return dst;
    }

    public static Rect getContourBoundingRect(MatOfPoint contour){
        return Imgproc.boundingRect(contour);
    }

    public static Mat cropMatByContourRect( Mat mat, MatOfPoint contour){
        Rect rect = Imgproc.boundingRect(contour);
//        Imgproc.rectangle(mat, rect.tl(), rect.br(), new Scalar(0, 0, 255));//, color, THICKNESS=1 or 2 ...);
//        updatePanelImage(grayscaleImage, resMat);
        System.out.println("Selecting Rectangle : [ " + rect.toString() + "]");

        Mat crop = new Mat();//new Mat(resMat, rect);
        Imgproc.cvtColor(new Mat(mat, rect), crop, Imgproc.COLOR_BGR2GRAY);
//        Imgcodecs.imwrite("./image_crop_gray.png", crop);

        return crop;
    }

    public static int findLargestContour ( List<MatOfPoint> contours ){
        int maxValIdx = 0;
        if (contours.size() >= 1) {
            double maxVal = 0;

            if (contours.size() > 1)
                for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
                    double contourArea = Imgproc.contourArea(contours.get(contourIdx));
                    if (maxVal < contourArea) {
                        maxVal = contourArea;
                        maxValIdx = contourIdx;
                    }
                }
            Rect rect = Imgproc.boundingRect(contours.get(maxValIdx));
//            Imgproc.rectangle(resMat, rect.tl(), rect.br(), new Scalar(0, 0, 255));//, color, THICKNESS=1 or 2 ...);
//            updatePanelImage(grayscaleImage, resMat);
            System.out.println("Selecting Rectangle : [ " + rect.toString() + "]");
        }
            return maxValIdx;
    }

    public static List<MatOfPoint> findContours(Mat maskedImage) {
// init
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
// find contours
        Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
//        System.out.println("\n\n!!! Contours Count : " + contours.size() + "\nContours MIN: " + contoursParams.getContourMinWidth() + " x " + contoursParams.getContourMinHeight() + " ==> " + contoursParams.getContourMinArea());
        System.out.println("\n\n!!! Contours Count : " + contours.size() );// + " : " + contoursParams.toString());
// filter contours found by expected minimal size params ( width, heigth, area )
//        List<MatOfPoint> contours2filter = new ArrayList<>();
//        for (MatOfPoint mp : contours) {
//            Rect rect = Imgproc.boundingRect(mp);
//            double length = rect.br().x - rect.tl().x;
//            double width = rect.br().y - rect.tl().y;
//            double area = length * width;
//            System.out.println("Area of Rectangle is [ " + rect.toString() + "] : " + area + "\n\n");
//            if (contoursParams.getContourMinArea() > area) {
//                contours2filter.add(mp);
//            }
//        }
//// above block - produces a list of contours, which might be filtered from resulting contours set
//        System.out.println("\n\n!!! Contours Count : " + contours.size() + " ==> Filtered : " + contours2filter.size());
// applying filtering to Contours list :
//        for (MatOfPoint contour : contours2filter) {
//            if ( contours.contains(contour) )
//                contours.remove(contour);
//        }
// if any contour exist - draw appropriate contours
//        if (hierarchy.size().height > 0 && hierarchy.size().width > 0) {
//            // for each contour, display it in blue
//            for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0]) {
//                Imgproc.drawContours(frame, contours, idx, new Scalar(250, 0, 0));
//            }
//        }
        return contours;
    }

    public static Mat loadImageFileToMat(String path){
        return Imgcodecs.imread(path, Imgcodecs.CV_LOAD_IMAGE_COLOR);// CV_LOAD_IMAGE_GRAYSCALE);
    }

    public static Mat processImgOpenCV(Mat image, OpenCvHsvModel hsvParams) {
//        Mat image = Imgcodecs.imread(file.getAbsolutePath(), Imgcodecs.CV_LOAD_IMAGE_COLOR);// CV_LOAD_IMAGE_GRAYSCALE);

//        StepEventBus.getEventBus().takeScreenshot();
//        Screenshot


        Mat blurredImage = new Mat();
        Mat hsvImage = new Mat();
        Mat mask = new Mat();
        Mat morphOutput = new Mat();
// remove some noise
        Imgproc.blur(image, blurredImage, new Size(7, 7));
// convert the frame to HSV
        Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);
// get thresholding values from the UI
// remember: H ranges 0-180, S and V range 0-255
        Scalar minValues = new Scalar(hsvParams.getHueStart(), hsvParams.getSaturationStart(),
                hsvParams.getValueStart());
        Scalar maxValues = new Scalar(hsvParams.getHueStop(), hsvParams.getSaturationStop(),
                hsvParams.getValueStop());
// show the current selected HSV range
        String valuesToPrint = "Hue range: " + minValues.val[0] + "-" + maxValues.val[0]
                + "\tSaturation range: " + minValues.val[1] + "-" + maxValues.val[1] + "\tValue range: "
                + minValues.val[2] + "-" + maxValues.val[2];
        System.out.println(valuesToPrint);
// threshold HSV image to select contours
        Core.inRange(hsvImage, minValues, maxValues, mask);
// show the partial output
//        updatePanelImage(maskImage, mask);
// morphological operators
// dilate with large element, erode with small ones
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12, 12));

        Imgproc.erode(mask, morphOutput, erodeElement);
        Imgproc.erode(morphOutput, morphOutput, erodeElement);

        Imgproc.dilate(morphOutput, morphOutput, dilateElement);
        Imgproc.dilate(morphOutput, morphOutput, dilateElement);

// show the partial output
//        updatePanelImage(morphImage, morphOutput);

//        resMat = findContours(mask, image);
//        updatePanelImage(grayscaleImage, resMat);

        return mask;
    }

    public static BufferedImage grabImage(WebDriver driver, WebElementFacade canvasElement, String outPath) {
// Get entire page screenshot
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        BaseStepListener baseListener = StepEventBus.getEventBus().getBaseStepListener();

//        File path = new File("./images/");
//        baseListener.getPhotographer()
//                .takesAScreenshot().with(driver).andWithBlurring(AnnotatedBluring.blurLevel())
//                .andSaveToDirectory(path.toPath());


// Get the location of element on the page
        Point canvasLocation = canvasElement.getLocation();
// Get width and height of the element
        int canvasWidth = canvasElement.getSize().getWidth();
        int canvasHeight = canvasElement.getSize().getHeight();

        BufferedImage fullImg = null;
        try {
            fullImg = ImageIO.read(screenshot);
//            ImageIO.write(fullImg, "png", new File("./images/123.png"));
//            ImageIO.write(fullImg.getSubimage(canvasLocation.getX(), canvasLocation.getY(),
//                    canvasWidth, canvasHeight), "png", new File(outPath));
        } catch (IOException e1) {
            e1.printStackTrace();
        }

// Crop the entire page screenshot to get only element screenshot
        BufferedImage eleScreenshot = fullImg.getSubimage(canvasLocation.getX(), canvasLocation.getY(),
                canvasWidth, canvasHeight);
        File outputfile = new File(outPath);
        try {
            ImageIO.write(eleScreenshot, "png", screenshot);
            ImageIO.write(eleScreenshot, "png", outputfile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

//        ImageIcon icon = new ImageIcon(eleScreenshot);
//        image = Imgcodecs.imread(outputfile.getAbsolutePath(), Imgcodecs.CV_LOAD_IMAGE_COLOR);// CV_LOAD_IMAGE_GRAYSCALE);
//        updatePanelImage(originalImage, image);

        return eleScreenshot;
    }
}
