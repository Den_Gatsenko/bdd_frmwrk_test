package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.pages.RegistrationPage;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.yecht.Data;

public class RegistrationSteps {

    RegistrationPage registrationPage;

    @Step
    public void openRegistrationPage(){
        registrationPage.open();
    }

    @Step
    public void register(String email, String password){


        registrationPage.setEmailField(email);
        registrationPage.setPasswordField(password);
        registrationPage.setConfirmPasswordField(password);
        registrationPage.submit();
    }
}
