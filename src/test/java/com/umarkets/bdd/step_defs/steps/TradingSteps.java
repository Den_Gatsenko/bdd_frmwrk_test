package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.models.OpenCvContoursModel;
import com.umarkets.bdd.models.OpenCvHsvModel;
import com.umarkets.bdd.pages.TradingPage;
import net.thucydides.core.annotations.Step;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Collections;
import java.util.List;

import static com.umarkets.bdd.helpers.OpenCvHelper.*;

public class TradingSteps {
    TradingPage tradingPage;
    private BufferedImage img;
    private OpenCvHsvModel params;
//    private Mat processedImgMat;
    private int rateContourIdx;
    private Mat crop;
    private List<MatOfPoint> contours;
    private Mat cropMat;
    private Mat binaryMat;
    private String ocr_res;
    private Mat maskMat;
    private Mat originalImageMat;

    @Step
    public void hideExtraCanvasLayers(){
        tradingPage.hideShowElementsExcept(Collections.singletonList(tradingPage.chartRateCanvas), true);
        tradingPage.hideShowElement(tradingPage.signalIndicatorCanvas, true);



        img = grabImage(tradingPage.getDriver(), tradingPage.chartRateCanvas, "./images/chart-rate-base.png");


        // Hue range: 0.0-180.0	Saturation range: 0.0-255.0	Value range: 50.0-255.0
        params = new OpenCvHsvModel(0, 0, 50, 180, 255, 255);

//        Hue range: 0.0-43.0	Saturation range: 0.0-138.0	Value range: 50.0-166.0
//        params = new OpenCvHsvModel(0, 0, 50, 45, 140, 166);
        originalImageMat = loadImageFileToMat("./images/chart-rate-base.png");
        maskMat = processImgOpenCV(originalImageMat, params);
        saveMatToImageFile(maskMat, "./images/chart-rate-mask.png");
    }

    @Step
    public void findCurrentRateContour(){
//        OpenCvContoursModel contoursParams = new OpenCvContoursModel();
        contours = findContours(maskMat);
        rateContourIdx =  findLargestContour(contours);
    }

    @Step
    public void cropCurrentRateContour(){
        cropMat = cropMatByContourRect(originalImageMat, contours.get(rateContourIdx) );
        saveMatToImageFile(cropMat, "./images/chart-rate-crop-gray.png");
    }

    @Step
    public void convertRateImageToBinary(){
        binaryMat = convertToBinaryImage(cropMat);
        saveMatToImageFile(binaryMat, "./images/chart-rate-processed-crop-binary.png");
    }

    @Step
    public void proceedOcr(){
        ocr_res = "0." + getOcrResult("./images/chart-rate-processed-crop-binary.png").substring(1);
    }

    @Step
    public void setHLine(Integer perc){
//        Integer perc = //Integer.valueOf(JOptionPane.showInputDialog(self, "Current indicator value: " + ocr_res + "\nPlease input percent from current value, for hLine to be set"));

        Float current = Float.valueOf(ocr_res) / 100000;
        Float hLiveVal = current + perc;// ((current / 100) * perc);
//        JOptionPane.showMessageDialog(self, "Going to set hLine frmo current : " + current + "\nTo new : " + hLiveVal);

//        for (WebElement el : canvasLayersElements) {
//            if (!el.equals(canvasElement)) {
//                hideShowElement(el, false);
//            }
//        }
        tradingPage.hideShowElementsExcept(Collections.singletonList(tradingPage.chartRateCanvas), false);
//        tradingPage.hideShowElement(tradingPage.signalIndicatorCanvas, false);
//        WebElement hLine = driver.findElement(By.id("hLine"));
//        hLine.click();
        tradingPage.selectHLineTool();


        Rect rect = getContourBoundingRect(contours.get(rateContourIdx));
        double width = rect.br().x - rect.tl().x;
        double height = rect.br().y - rect.tl().y;
        int centerX = (int) (rect.tl().x + width / 2);
        int centerY = (int) (rect.tl().y + height % 2);
        Actions actionBuilder = new Actions(tradingPage.getDriver());
        Action drawOnCanvas = actionBuilder
                .moveToElement(tradingPage.chartRateCanvas, centerX - 100, (int) (centerY - hLiveVal))
                .click()
                .build();
        drawOnCanvas.perform();
    }
}
