package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.pages.LoginPage;
import com.umarkets.bdd.pages.LoginTradingAccountPage;
import net.thucydides.core.annotations.Step;
import org.yecht.Data;

import static org.junit.Assert.assertEquals;

public class LoginTradingAccountSteps {
    LoginTradingAccountPage loginTradingAccountPage;

    @Step
    public void openLoginPage(){
        loginTradingAccountPage.open();
    }

    @Step
    public void logIn(String email, String password){
        loginTradingAccountPage.setEmailField(email);
        loginTradingAccountPage.setPasswordField(password);
        loginTradingAccountPage.submitLogin();
    }

    @Step
    public void verifyLoggedIn(String userName){
//        assertEquals("My Account", loginPage.$("//a[contains(@href, '#myaccount')]").getText());
        assertEquals("Welcome: " + userName, loginTradingAccountPage.getWellcomeUserText());
    }
}
