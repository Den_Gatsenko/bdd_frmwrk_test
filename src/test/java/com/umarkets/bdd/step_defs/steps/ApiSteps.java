package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.helpers.UsersStorage;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.yecht.Data;

import static net.serenitybdd.rest.SerenityRest.given;
import static net.serenitybdd.rest.SerenityRest.then;
import static net.serenitybdd.rest.SerenityRest.with;

public class ApiSteps {

    String API_BASE_URL = "https://test-api.umarkets.com";
    String API_REGISTRATION_ENDPOINT = "/Registration/account"; // #!/Registration/Registration_account
    String API_LOGIN_ENDPOINT = "/Account/logon";

    @Step
    public void api_login(String email, String password){
        System.out.println("email = [" + email + "], password = [" + password + "]");
//        given().contentType("application/json")
//                .and().body()
        Response response = with().parameters(
                "email", email,
                "password", password)
                .post(API_BASE_URL + API_LOGIN_ENDPOINT);
        then().assertThat().statusCode(200).assertThat().contentType("application/json");


        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        String myCustomProperty = variables.getProperty("my.custom.property");
        System.out.println("\n\n\n!!!!!!! myCustomProperty = [" + myCustomProperty + "]\n\n\n");

        ValidatableResponse jsonResponse = response.then();
        String jsonStrResponse = jsonResponse.extract().asString();
        dumpHeaders(response.headers());
        System.out.println("response: \n" + jsonStrResponse + "\n");
    }

    public void api_register(UsersStorage.USER_STORAGE_DATA user) {
        api_register(user.getEmail(), user.getPassword());
    }

    @Step
    public void api_register(String email, String password) {
        System.out.println("email = [" + email + "], password = [" + password + "]");
        Response response = (Response) with().parameters(
                "model.email", email,
                "model.password", password)
                .get(API_BASE_URL + API_REGISTRATION_ENDPOINT);
        ValidatableResponse jsonResponse = then().assertThat().statusCode(200);

//        ValidatableResponse jsonResponse = response.then();
        String jsonStrResponse = jsonResponse.extract().asString();
        dumpHeaders(response.headers());
        System.out.println("response: \n" + jsonStrResponse + "\n");

        System.out.println("AUTH Cookie : " + response.getCookie(".ASPXAUTHAPI"));
    }

    public static void dumpHeaders(Headers headers){
        // Iterate over all the Headers
        for(Header header : headers) {
            System.out.println("Key: " + header.getName() + " Value: " + header.getValue());
        }
    }
}
