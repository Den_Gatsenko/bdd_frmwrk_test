package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.pages.CommonNavigationArea;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class NavigationSteps {
    CommonNavigationArea navigationArea;

    @Step
    public void navigateTo ( String area ){
        navigationArea.navigateTo ( area );
    }
}
