package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.pages.LoginPage;
import net.thucydides.core.annotations.Step;

import static org.junit.Assert.assertEquals;

public class LoginSteps {
    LoginPage loginPage;

    @Step
    public void openLoginPage(){
        loginPage.open();
    }

    @Step
    public void logIn(String email, String password){
        loginPage.setEmailField(email);
        loginPage.setPasswordField(password);
        loginPage.submitLogin();
    }

    @Step
    public void verifyLoggedIn(){
        assertEquals("My Account", loginPage.$("//a[contains(@href, '#myaccount')]").getText());
    }

}
