package com.umarkets.bdd.step_defs.steps;

import com.umarkets.bdd.pages.CompleteRegistrationPage;
import net.thucydides.core.annotations.Step;

import static com.umarkets.bdd.helpers.Randomizer.getRandomName;
import static com.umarkets.bdd.helpers.Randomizer.getRandomNumber;
import static org.junit.Assert.assertEquals;

public class CompleteRegistrationSteps {

    CompleteRegistrationPage completeRegistrationPage;

    @Step
    public void verifyOnCompleteRegistrationPage(){
        completeRegistrationPage.isOnCompleteRegistrationPage();
    }

    @Step
    public void fillCompleteRegistrationForm(){
        String firstName = getRandomName();
        String lastName = getRandomName();
//        String country =
        String city = getRandomName();
        String phoneCode = getRandomNumber(4);
        String phoneNumber = getRandomNumber(8 );

        completeRegistrationPage.setFirstNameField(firstName);
        completeRegistrationPage.setLastNameField(lastName);
        completeRegistrationPage.setRandomCountry();// setCountryIdSelect(country);
        completeRegistrationPage.setCityField(city);
        completeRegistrationPage.setPhoneCountryCode(phoneCode);
        completeRegistrationPage.setPhoneNumber(phoneNumber);
        completeRegistrationPage.setTermsAccepted(true);
        completeRegistrationPage.clickContinue();
    }
}
