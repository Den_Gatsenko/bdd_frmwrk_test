package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.step_defs.steps.TradingSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class TradingStepDefs extends CommonStepDefs {
//    @Given("^a logged in Client$")
//    public void a_logged_in_Client() throws Exception {
//        // Write code here that turns the phrase above into concrete actions
//        throw new PendingException();
//    }



    @When("^Client checks current rate$")
    public void client_checks_current_rate() throws Exception {
        tradingSteps.hideExtraCanvasLayers();
        tradingSteps.findCurrentRateContour();
        tradingSteps.cropCurrentRateContour();
        tradingSteps.convertRateImageToBinary();
        tradingSteps.proceedOcr();
    }

    @Then("^Client sets hLine \"([^\"]*)\" point above current rate$")
    public void client_sets_hLine_point_above_current_rate(String arg1) throws Exception {
        tradingSteps.setHLine(Integer.valueOf(arg1));
    }
}
