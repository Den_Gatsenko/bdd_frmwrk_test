package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.helpers.UsersStorage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import cucumber.api.java8.En;
import net.serenitybdd.core.Serenity;


public class LoginTradingAccountStepDefs extends CommonStepDefs {

    private UsersStorage.USER_STORAGE_DATA user;

//    public LoginTradingAccountStepDefs() {
//        Given("^a logged in Client$",this::client_should_be_logged_in);
//        Then("^Client should be logged in$", this::client_should_be_logged_in);
//    }

    @Given("^a Client is on \"([^\"]*)\" page$")
    public void a_Client_is_on_page(String arg1) throws Exception {
        loginTradingAccountSteps.openLoginPage();
    }

    @When("^he logs in as \"([^\"]*)\"/\"([^\"]*)\"$")
    public void he_logs_in_as(String arg1, String arg2) throws Exception {
        user = new UsersStorage.USER_STORAGE_DATA(arg1, arg2);
        System.out.println("new user : " + user.toString());
        Serenity.setSessionVariable("NEW_USER").to(user);
        loginTradingAccountSteps.logIn(arg1, arg2);
    }

//    public clientLoggedInVerificationLambdaStepdefs() throws Exception {
//        @When( "^Client should be logged in$",this::client_should_be_logged_in);
//        @When("^I add the numbers (\\d+) and (\\d+)$", this::performXPlusY);
//    }
//    private void performXPlusY(int a, int b){
//        // do the step
//    }

//    @Given("^a logged in Client$")
    @Then("^(?:a logged in Client|Client should be logged in)$")
    public void client_should_be_logged_in() throws Exception {
        loginTradingAccountSteps.verifyLoggedIn(user.getEmail());
    }

}
