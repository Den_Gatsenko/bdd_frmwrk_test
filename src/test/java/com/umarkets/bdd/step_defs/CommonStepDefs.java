package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.helpers.UsersStorage;
import com.umarkets.bdd.step_defs.steps.*;
import net.thucydides.core.annotations.Steps;

public class CommonStepDefs {

    @Steps
    TradingSteps tradingSteps;

    @Steps
    LoginSteps loginSteps;

    @Steps
    LoginTradingAccountSteps loginTradingAccountSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    CompleteRegistrationSteps completeRegistrationSteps;

    @Steps
    RegistrationSteps registrationSteps;

    @Steps
    ApiSteps apiSteps;

    UsersStorage.USER_STORAGE_DATA newUser;
}
