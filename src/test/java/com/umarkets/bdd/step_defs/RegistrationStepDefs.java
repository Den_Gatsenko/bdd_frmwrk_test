package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.helpers.UsersStorage;
import com.umarkets.bdd.pages.RegistrationPage;
import com.umarkets.bdd.step_defs.steps.ApiSteps;
import com.umarkets.bdd.step_defs.steps.LoginSteps;
import com.umarkets.bdd.step_defs.steps.RegistrationSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.yecht.Data;

import static com.umarkets.bdd.helpers.Randomizer.getRandomEmail;
import static com.umarkets.bdd.helpers.Randomizer.getRandomPassword;
import static com.umarkets.bdd.helpers.Randomizer.parseTemplate;
import static com.umarkets.bdd.helpers.UsersStorage.storeNewUser;

public class RegistrationStepDefs extends CommonStepDefs {

    @Given("^A Client want to register with our service via \"([^\"]*)\"$")
    public void aClientWantToRegisterWithOurServiceVia(String arg0) throws Throwable {
        switch (arg0){
            case "REST API" : {
                Serenity.setSessionVariable("NEW_USER_API").to(true);
                break;
            }
            default: Serenity.setSessionVariable("NEW_USER_API").to(false);
        }
    }


    @When("^He provides unique email address and password for registration$")
    public void he_provides_unique_email_address_and_password_for_registration() throws Exception {
        newUser = new UsersStorage.USER_STORAGE_DATA(getRandomEmail(), getRandomPassword());
        System.out.println("new user : " + newUser.toString());
        Serenity.setSessionVariable("NEW_USE    R").to(newUser);
    }


    @When("^He submit registration data$")
    public void heSubmitRegistrationData() throws Throwable {
        if ( ! (Boolean.valueOf(Serenity.sessionVariableCalled("NEW_USER_API").toString()))  ) {
            registrationSteps.openRegistrationPage();
            registrationSteps.register(newUser.getEmail(), newUser.getPassword());
        } else
            apiSteps.api_register(newUser.getEmail(), newUser.getPassword());
        storeNewUser(newUser);
    }

    @Then("^He should be registered$")
    public void he_should_be_registered() throws Exception {
        if ( ! (Boolean.valueOf(Serenity.sessionVariableCalled("NEW_USER_API").toString()))  ) {
            loginSteps.verifyLoggedIn();
        } else
            apiSteps.api_login(newUser.getEmail(), newUser.getPassword());
    }







    @Given("^A Client want to register with our service$")
    public void a_Client_want_to_register_with_our_service() throws Exception {
        UsersStorage.USER_STORAGE_DATA newUser = new UsersStorage.USER_STORAGE_DATA(getRandomEmail(), getRandomPassword());
        System.out.println("new user : " + newUser.toString());
        Serenity.setSessionVariable("NEW_USER").to(newUser);

        registrationSteps.openRegistrationPage();
    }







    @Given("^a user at \"([^\"]*)\" page$")
    public void a_user_at_page(String arg1) throws Exception {
        registrationSteps.openRegistrationPage();
    }

    @When("^user register using email (.*) and password (.*)$")
    public void user_register_using_email_and_password(String arg1, String arg2) throws Exception {
        String email = parseTemplate(arg1);
        String password = parseTemplate(arg2);
        System.out.println("arg1 = [" + arg1 + "], arg2 = [" + arg2 + "]");
        System.out.println("email = [" + email + "], password = [" + password + "]");

        registrationSteps.register(email,password);
    }

    @Then("^user at \"([^\"]*)\" page$")
    public void user_at_page(String arg1) throws Exception {
        loginSteps.verifyLoggedIn();
    }


}
