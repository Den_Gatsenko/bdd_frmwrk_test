package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.helpers.UsersStorage;
import com.umarkets.bdd.step_defs.steps.ApiSteps;
import com.umarkets.bdd.step_defs.steps.LoginSteps;
import com.umarkets.bdd.step_defs.steps.RegistrationSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static com.umarkets.bdd.helpers.Randomizer.getRandomEmail;
import static com.umarkets.bdd.helpers.Randomizer.getRandomPassword;
import static com.umarkets.bdd.helpers.Randomizer.getRandomUser;

public class LoginStepDefs extends CommonStepDefs {

    @Given("^A registered Client want to log in to Umarkets account via \"([^\"]*)\"$")
    public void aRegisteredClientWantToLogInToUmarketsAccount(String arg0) throws Throwable {
        switch (arg0){
            case "REST API" : {
                Serenity.setSessionVariable("LOGIN_API").to(true);
                break;
            }
            default: Serenity.setSessionVariable("LOGIN_API").to(false);
        }
    }

    @When("^He submit login credentials$")
    public void heSubmitLoginCredentials() throws Throwable {
        newUser = Serenity.sessionVariableCalled("NEW_USER");
        if ( null == newUser )
            newUser = getRandomUser();
        if ( ! (Boolean.valueOf(Serenity.sessionVariableCalled("LOGIN_API").toString()))  ) {
            loginSteps.openLoginPage();
            loginSteps.logIn(newUser.getEmail(), newUser.getPassword());
        } else
            apiSteps.api_login(newUser.getEmail(), newUser.getPassword());
    }




    @Given("^a web browser at \"([^\"]*)\"$")
    public void a_web_browser_at(String arg1) throws Exception {
        loginSteps.openLoginPage();
    }

    @When("^user logs in using email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void user_logs_in_using_email_and_password(String arg1, String arg2) throws Exception {
        UsersStorage.USER_STORAGE_DATA user = new UsersStorage.USER_STORAGE_DATA(arg1, arg2);
        System.out.println("new user : " + user.toString());
        Serenity.setSessionVariable("NEW_USER").to(user);
        loginSteps.logIn(arg1,arg2);
    }

    @Then("^user should be signed in$")
    public void user_should_be_signed_in() throws Exception {
        loginSteps.verifyLoggedIn();
    }

    @Then("^page \"([^\"]*)\" should be visible$")
    public void page_should_be_visible(String arg1) throws Exception {
        System.out.println("arg1 = [" + arg1 + "]");
    }

    @Given("^A newly registered Client logged in to a system$")
    public void aNewlyRegisteredClientLoggedInToASystem() throws Throwable {
        newUser = getRandomUser(); //Serenity.sessionVariableCalled("NEW_USER");
        apiSteps.api_register(newUser);
        Serenity.setSessionVariable("NEW_USER").to(newUser);
        System.out.println("new user : " + newUser.toString());

//        registrationSteps.openRegistrationPage();
        loginSteps.openLoginPage();
        loginSteps.logIn(newUser.getEmail(), newUser.getPassword());
    }

    @Then("^He should be logged in$")
    public void heShouldBeLoggedIn() throws Throwable {
        if ( ! (Boolean.valueOf(Serenity.sessionVariableCalled("LOGIN_API").toString()))  ) {
            loginSteps.verifyLoggedIn();
        } else
            apiSteps.api_login(newUser.getEmail(), newUser.getPassword());
    }



}
