package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.helpers.UsersStorage;
import com.umarkets.bdd.step_defs.steps.CompleteRegistrationSteps;
import com.umarkets.bdd.step_defs.steps.LoginSteps;
import com.umarkets.bdd.step_defs.steps.NavigationSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static com.umarkets.bdd.helpers.Randomizer.getRandomName;

public class CompleteRegistrationStepDefs extends CommonStepDefs {



    @Given("^An already registered Client$")
    public void an_already_registered_Client() throws Exception {
        newUser = Serenity.sessionVariableCalled("NEW_USER");
    }

    @When("^He logs in to a system$")
    public void he_logs_in_to_a_system() throws Exception {
        loginSteps.logIn(newUser.getEmail(),newUser.getPassword());
    }

    @When("^He navigates to any section of a site$")
    public void he_navigates_to_any_section_of_a_site() throws Exception {
        loginSteps.verifyLoggedIn();
        navigationSteps.navigateTo("Deposit");
    }

    @When("^He prompted to finish registration$")
//    @Then("^He should be prompted to finish registration$")
    public void hePromptedToFinishRegistration() throws Throwable {
        navigationSteps.navigateTo("Deposit");
    }

    @Then("^He should fill form to finish registration$")
    public void heShouldFillFormToFinishRegistration() throws Throwable {
        completeRegistrationSteps.fillCompleteRegistrationForm();
    }

    @Then("^He should be prompted to finish registration$")
    public void heShouldBePromptedToFinishRegistration() throws Throwable {
        completeRegistrationSteps.verifyOnCompleteRegistrationPage();
    }
}
