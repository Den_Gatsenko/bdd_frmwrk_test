package com.umarkets.bdd.step_defs;

import com.umarkets.bdd.helpers.UsersStorage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

import java.net.URI;
import java.net.URL;
import java.util.List;

import static com.umarkets.bdd.helpers.Randomizer.getRandomEmail;
import static com.umarkets.bdd.helpers.Randomizer.getRandomPassword;
import static net.serenitybdd.rest.SerenityRest.rest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ApiStepDefs extends CommonStepDefs{

    String baseURL;
    String response;


    @Given("^user use API at base URL \"([^\"]*)\"$")
    public void user_use_API_at_base_URL(String arg1) throws Exception {
        baseURL = arg1;
        System.out.println("arg1 = [" + arg1 + "]");
    }

    @When("^request is made to endpoint \"([^\"]*)\"$")
    public void request_is_made_to_endpoint(String arg1) throws Exception {
        response = rest().get(new URL(baseURL+arg1)).asString();
        System.out.println("arg1 = [" + arg1 + "]");
        System.out.println("response:\n" + response);
    }

    @Then("^user should receive a list of available feeds$")
    public void user_should_receive_a_list_of_available_feeds() throws Exception {
        List<String> feeds = JsonPath.from(response).getList("feeds");
        System.out.println("feeds:\n" + feeds);
        assertThat(feeds.size(), equalTo(9));
    }
}
