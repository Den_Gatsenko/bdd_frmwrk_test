package com.umarkets.bdd.hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class Hooks {
    public static Scenario scenario;

    @Before
    public void keepScenario(Scenario scenario){
        this.scenario = scenario;

        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

        String baseUrl = variables.getProperty(ThucydidesSystemProperty.WEBDRIVER_BASE_URL);
        String myCustomProperty = variables.getProperty("my.custom.property");

        System.out.println("baseUrl = [" + baseUrl + "]");
        scenario.write("@Before: baseUrl = [" + baseUrl + "]");
        System.out.println("myCustomProperty = [" + myCustomProperty + "]");
        scenario.write("@Before: myCustomProperty = [" + myCustomProperty + "]");
    }


//    @Before
//    public void prepareData() {
//        System.out.println("@Before: prepareData");
////        scenario.write("@Before: prepareData");
//    }

    @After
    public void clearData() throws IOException {
        System.out.println("@After: clearData");

        File[] pngFiles = getFilesInDirectory("./images/", ".png");
        List<String> fileNames = new ArrayList<>();
        if (pngFiles.length > 0) {
            for (File file : pngFiles) {
                if (file.isFile() & (!file.toString().contains("scaled_"))) {
                    fileNames.add(file.toString());
                }
            }
            byte[] byteImg = extractBytes(fileNames.get(fileNames.size() - 1));
            scenario.embed(byteImg, "image/png");

            scenario.write("@After: clearData");
        }
    }

    public static byte[] extractBytes (String ImageName) throws IOException {
        byte[] bytes = new byte[0];
        try {
            RandomAccessFile f = new RandomAccessFile(ImageName, "r");
            bytes = new byte[(int) f.length()];
            f.read(bytes);
            f.close();
            return bytes;
        }catch (Exception e){
            return bytes;
        }
    }

    public static File[] getFilesInDirectory(String folder, String ext) {
        File[] txts = new File(folder).listFiles(
                (dir, name) -> {
                    return name.toLowerCase().endsWith(ext);
                }
        );
        return txts;
    }
}